
#include <iostream>
#include <sstream>
#include "muson/muson.h"
#include "muson/muson_to_json.h"

int main() {

    using People = muson::Signature<LIT("[{name:String,isAlive:?Bool,age:Int}]")>;

    static const std::string sig = People::print_signature();

    struct accessor {
        struct val_t {
            std::string name;
            struct {
                bool null;
                bool data;
            } isAlive;
            int64_t age;
        };
        std::vector<val_t> data;
    };

    std::string input;
    input.assign(std::istreambuf_iterator<char>(std::cin), std::istreambuf_iterator<char>());
   
    try { 
        People parsed;
        muson::decode(sig, input, parsed);

        accessor& x = muson::cast<accessor>(parsed);

        for (const auto& i : x.data) {
            std::cout << i.name << "\t"
                      << (i.isAlive.null ? "Maybe" : i.isAlive.data ? "Yes" : "No") << "\t"
                      << i.age << std::endl;
        }

        std::ostringstream ss;
        muson::to_string_json(ss, parsed);

        std::cout << ss.str() << std::endl;

    } catch (std::exception& e) {
        std::cout << "Oops: " << e.what() << std::endl;
    } catch (...) {
        std::cout << "Oops." << std::endl;
    }
    return 0;
}
