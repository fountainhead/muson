
#include <iostream>
#include <sstream>
#include "muson/muson.h"

int main() {

    using People = muson::Signature<LIT("[{name:String,isAlive:?Bool,age:Int}]")>;

    static const std::string sig = People::print_signature();

    struct accessor {
        struct val_t {
            std::string name;
            struct {
                bool null;
                bool data;
            } isAlive;
            int64_t age;

            val_t(const char* c) : name(c), age(0) { isAlive = {false, true}; }
        };
        std::vector<val_t> data;
    };

    accessor data;

    data.data.emplace_back("One");
    data.data.emplace_back("Two");
    data.data.emplace_back("Three");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("Four");
    data.data.emplace_back("One");
    data.data.emplace_back("Two");

    try {
        std::ostringstream ss;
        muson::encode(sig, ss, muson::cast<People>(data));

        std::cout << ss.str() << std::endl;

    } catch (std::exception& e) {
        std::cout << "Oops: " << e.what() << std::endl;
    } catch (...) {
        std::cout << "Oops." << std::endl;
    }
    return 0;
}
