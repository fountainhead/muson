#ifndef __FMT_H
#define __FMT_H

#include <math.h>

namespace fmt {

template <typename T> inline void _numeric_sign(std::string& out, T& t) {
    if (t < 0) {
        out += '-';
    }
}

template <typename T>
inline std::string& format_numeric(std::string& out, T t, int base = 10, int pad = 0, bool trim = false) {
    const char* base_ = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    char buff[65];
    int i = 0;

    if (base < 2 || base > 36) base = 10;

    if (out.capacity() - out.size() < 12) {
        out.reserve( out.capacity() + 12 );
    }

    _numeric_sign(out, t);

    int mult = (t < 0) ? -1 : 1;

    while (i < 64) {
        int p = (t % base) * mult;

        buff[i] = base_[p];
        t /= base;
        if (t == 0) break;
        i++;
    }

    while (pad > i+1) {
        out += '0';
        pad--;
    }

    int start = 0;
    while (trim && buff[start] == '0') {
        ++start;
    }

    while (i >= start) {
        out += buff[i];
        i--;
    }

    return out;
}

template <typename T> inline T __round(T);
template <> inline float       __round(float       x) { return roundf(x); }
template <> inline double      __round(double      x) { return round(x);  }
template <> inline long double __round(long double x) { return roundl(x); }

template <typename T> inline T __isinf(T);
template <> inline float       __isinf(float       x) { return ::isinf(x); }
template <> inline double      __isinf(double      x) { return ::isinff(x);  }
template <> inline long double __isinf(long double x) { return ::isinfl(x); }

template <typename T>
inline std::string& format_real(std::string& out, T t, bool trim = false, int round = 5) {

    if (isnan(t)) {
        out += "nan";
        return out;
    }

    if (__isinf(t)) {
        if (__isinf(t) < 0) out += '-';
        out += "inf";
        return out;
    }

    long l = (long)t;

    T t0 = t - l;

    if (t0 < 0) {
        t0 = -t0;
        l = -l;
        out += '-';
    }

    // HACK!
    if (t0 > 1.0) {
        out += "nan";
        return out;
    }

    if (round > 15) round = 15;

    long whole = 1;

    for (int i = 0; i < round; ++i) {
        t0 *= 10;
        whole *= 10;
    }

    long f = (long)__round(t0);

    if (f >= whole) {
       l++;
       f -= whole;
    }

    format_numeric(out, l);

    if (!trim || f > 0) {
        out += '.';
        format_numeric(out, f, 10, round, trim);
    }

    return out;
}

template <typename T, typename I>
T parse(I i) {

    int mul = 1;

    if (*i == '-') {
        mul = -1;
        ++i;
    }

    T ret = 0;

    while (*i >= '0' && *i <= '9') {
        int x = *i - '0';
        ret *= 10;
        ret += x;
        ++i;
    }

    return ret * mul;
}

}

#endif
