import sys
import os
import json

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')

import muson

def go(m):
    print(json.dumps(muson.uncompress(m)[1]))

if __name__=='__main__':
    go(sys.stdin.read())

