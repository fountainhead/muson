import sys
import os
import json

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')

import muson

def go(signature, j):
    print(muson.compress(signature, j))

if __name__=='__main__':
    go(sys.argv[1], json.load(sys.stdin))

