
This is an API documentation for [muson.h](https://bitbucket.org/tkatchev/muson/src/HEAD/muson/muson.h?at=master)

## Public API

    :::c++
    #include "muson.h"

The core functionality comes in a single header file.

######

    :::c++
    using T = muson::Signature<LIT(x)>

Here `x` is a literal (double-quoted) string. This is a C++ type that results from parsing the given string as a μ-son signature. (Note: the parsing is done at compile time!)

######

    :::c++
    std::string T::print_signature()

Here `T` is a `muson::Signature<>`. A static function that returns the corresponding μ-son signature.

######

    :::c++
    void muson::decode(const std::string& Tsig, R& reader, T& holder)

Here `Tsig` is a μ-son signature, `R` is a type implementing the interface of a `muson::Reader<I>` and `T` is a `muson::Signature<>`. The parsed data will be into `holder`.

######

    :::c++
    void muson::decode(const std::string& Tsig, const std::string& data, T& holder)

As above, but a default reader will be used. `data` is the input μ-son document.

######

    :::c++
    void muson::encode(const std::string& Tsig, W& writer, const T& holder)

`T` is a `muson::Signature<>`, `W` is a type that implements `operator<<` for integers, doubles, chars and strings. (For example, `std::stringstream`.)

######

    :::c++
    Reader<I>

Is a class implementing a μ-son reader. `I` is an iterator for input bytes. Two public methods can be overriden:

######

    :::c++
    int64_t Reader<I>::to_int(const I& b)

Parses the bytes starting at `b` into an integer.

######

    :::c++
    double Reader<I>::to_real(const I& b)

Same, except for doubles.

######

    :::c++
    A& muson::cast<A>(T& t)

A way of casting aliased types, for casting a `muson::Signature<>` to another layout-compatible type.

######

A `muson::Signature<>` is a composition of types that are layout-compatible to these:

    struct Null {};

    struct Bool {
        bool data;
    };

    struct Int {
        int64_t data;
    }

    struct Real {
        double data;
    }

    struct String {
        std::string data;
    }

    template <typename T>
    struct Optional {
        bool null;
        T data;
    };

    template <typename T>
    struct List {
        std::vector<T> data;
    };

    template <typename T>
    struct Object {
        T data;
    };

    template <typename T, typename S...>
    struct Object {
        T data;
        Object<S...> tail;
    };

######

    :::c++
    X& muson::get<LIT(f)>(const T& t)

Here `T` is a `muson::Object`, `f` is a string literal (double-quoted) and `X` is some unspecified type. Returns the value of the given field from the given object.

######

    :::c++
    MUGET(t, f)

A macro that's equivalent to `muson::get<LIT(f)>(t)`.

## Internal stuff

    :::c++
    LETTERS(x)

Is a macro that expands the double-quoted string literal `x` into a sequence of its chars separated by commas.

######

    :::c++
    template <char C...>
    struct Literal {
        static constexpr const char vals[] = { C..., '\0' };
    };

A template for holding a string of characters. (I.e., a compile-time string value.)

######

    :::c++
    template <char C...>
    struct Cleaned {
        ...
    };

A type alias for a `Literal<>` except with trailing zero bytes in `C...` removed.

######

    :::c++
    LIT(x)

A macro that is equivalent to `muson::Cleaned<LETTERS(c)>`.

######

A `muson::Object` actually looks like this:

    Object< Seq< Pair<Literal<...>, T1>,
                 Pair<Literal<...>, T2>,
                 ... > >

`Pair` and `Seq` are internal types that do not affect struct layout.
