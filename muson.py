#!/usr/bin/env python2

import json
import sys

global iteritems

if sys.version_info[0] >= 3:
    global unicode
    global xrange
    unicode = str
    iteritems = lambda x: x.items()
    xrange = range
else:
    iteritems = lambda x: x.iteritems()

class Types:
    Object = 1
    List = 2
    Optional = 3
    Keyword = 4
    String = 5
    Int = 6
    Real = 7
    Bool = 8
    Null = 9

class Tokenizer:
    def __init__(self, s):
        self.str = s
        self.i = 0

    def consume(self, p):
        if self.str.startswith(p, self.i):
            self.i += len(p)
            return True
        return False

    def skipto(self, chars):
        oldi = self.i
        while self.i < len(self.str) and self.str[self.i] not in chars:
            self.i += 1
        return self.str[oldi:self.i]

    def pop(self, n=1):
        oldi = self.i
        self.i += n
        return self.str[oldi:self.i]

def signature_print(s):
    i = s[0]
    if i == Types.Object:
        return '{' + ','.join(signature_print(i) for i in s[1]) + '}'
    elif i == Types.List:
        return '[' + signature_print(s[1]) + ']'
    elif i == Types.Optional:
        return '?' + signature_print(s[1])
    elif i == Types.Keyword:
        return s[1] + ':' + signature_print(s[2])
    elif i == Types.String:
        return 'String'
    elif i == Types.Int:
        return 'Int'
    elif i == Types.Real:
        return 'Real'
    elif i == Types.Bool:
        return 'Bool'
    elif i == Types.Null:
        return 'Null'
    else:
        raise Exception('Sanity error, invalid signature: ' + str(s))

def signature_parse(tokenizer):
    if tokenizer.consume('String'):
        return (Types.String,)

    if tokenizer.consume('Int'):
        return (Types.Int,)

    if tokenizer.consume('Real'):
        return (Types.Real,)

    if tokenizer.consume('Bool'):
        return (Types.Bool,)

    if tokenizer.consume('Null'):
        return (Types.Null,)

    if tokenizer.consume('?'):
        return Types.Optional, signature_parse(tokenizer)

    if tokenizer.consume('['):
        t = signature_parse(tokenizer)
        if not tokenizer.consume(']'):
            raise Exception('Invalid signature: ' + tokenizer.str)
        return Types.List, t

    if tokenizer.consume('{'):
        ret = []
        while not tokenizer.consume('}'):
            kw = tokenizer.skipto(':')
            if not tokenizer.consume(':'):
                raise Exception('Invalid signature: ' + tokenizer.str)
            t = signature_parse(tokenizer)
            ret.append((Types.Keyword, kw, t))
            tokenizer.consume(',')

        return Types.Object, ret

    raise Exception('Invalid signature: "' + tokenizer.str + '".')

def signature_infer(j):

    def object_key_sorter(a):
        # Sort fields in an object so that mandatory fields come before optional fields.
        # This is required because otherwise the cases 'absent object' and 'present object with absent field' are
        # encoded ambiguously.
        # Test case:
        #
        #  [{"error":"Could not connect"}, {"response":{"status":200, "content":"Hello world!"}}, {"response":{"status":502}}]

        is_optional_keyword = (len(a) == 3 and a[0] == Types.Keyword and len(a[2]) == 2 and a[2][0] == Types.Optional)
        return (is_optional_keyword, a)

    def unify(a, b):
        errmsg = Exception('Cannot unify types ' + signature_print(a) + ' and ' + signature_print(b) + '.')

        if a == b:
            return a

        if a[0] == Types.Null:
            if b[0] == Types.Optional:
                return b
            else:
                return Types.Optional, b

        if b[0] == Types.Null:
            if a[0] == Types.Optional:
                return a
            else:
                return Types.Optional, a

        if a[0] == Types.Optional or b[0] == Types.Optional:
            if a[0] == Types.Optional and b[0] == Types.Optional:
                return Types.Optional, unify(a[1], b[1])
            elif a[0] == Types.Optional:
                return Types.Optional, unify(a[1], b)
            elif b[0] == Types.Optional:
                return Types.Optional, unify(a, b[1])

        if a[0] != b[0]:
            raise errmsg

        elif a[0] == Types.List:
            return Types.List, unify(a[1], b[1])

        elif a[0] == Types.Object:

            combined = {}
            for _, k, t in a[1]:
                combined[k] = [t, (Types.Null,)]

            for _, k, t in b[1]:
                if k in combined:
                    combined[k][1] = t
                else:
                    combined[k] = [(Types.Null,), t]

            ret = []
            for k, v in iteritems(combined):
                ret.append((Types.Keyword, k, unify(v[0], v[1])))

            return Types.Object, sorted(ret, key=object_key_sorter)

    if type(j) == str or type(j) == unicode:
        return (Types.String,)

    elif type(j) == int:
        return (Types.Int,)

    elif type(j) == float:
        return (Types.Real,)

    elif type(j) == bool:
        return (Types.Bool,)

    elif j is None:
        return (Types.Null,)

    elif type(j) == dict:
        fields = []

        for k,v in iteritems(j):
            if type(k) != str and type(k) != unicode:
                raise Exception('Only string keys are allowed.')
            if k.find(':') >= 0:
                raise Exception('The ":" symbol is not allowed in object attribute names.')

            fields.append((Types.Keyword, k, signature_infer(v)))

        return Types.Object, sorted(fields)

    elif type(j) == list:

        if len(j) == 0:
            return Types.List, (Types.Null,)

        i = 0
        merged = signature_infer(j[i])
        i += 1
        while i < len(j):
            merged = unify(merged, signature_infer(j[i]))
            i += 1

        return Types.List, merged

    else:
        raise Exception('Invalid type of JSON value: ' + str(j))

####

class State:
    class Cache:
        def __init__(self):
            self.log = []

        def put_raw(self, v, n = 10):
            self.log.append(v)
            while len(self.log) > 10:
                del self.log[0]

        def put(self, v, n = 10):
            ret = -1
            for i in xrange(len(self.log)):
                if self.log[i] == v:
                    ret = len(self.log) - 1 - i
                    self.log.pop(i)
                    break
            self.put_raw(v, n)
            return ret

        def get(self, n):
            ret = self.log[-1 - n]
            del self.log[-1 - n]
            return ret

    def __init__(self):
        tmp = self.Cache()
        self.cache = { str: tmp, unicode: tmp, int: self.Cache(), float: self.Cache() }

    def put(self, v):
        return self.cache[type(v)].put(v)

    def put_raw(self, v):
        return self.cache[type(v)].put_raw(v)

    def get(self, typ, n):
        return self.cache[typ].get(n)

class Compressed:
    def __init__(self, data = '', null_head = False):
        self.data = data
        self.null_head = null_head

    def append(self, other):
        self.data += other.data
        return self

    def prepend(self, s):
        self.data = s + self.data
        return self

def data_compress(sig, json, state):
    i = sig[0]
    if i == Types.Object:
        ret = None
        for _, k, v in sig[1]:
            tmp = data_compress(v, json.get(k, None), state)
            ret = tmp if not ret else ret.append(tmp)
        return ret

    elif i == Types.List:
        ret = '['
        for x in json:
            ret += data_compress(sig[1], x, state).data
        ret += ']'
        return Compressed(ret)

    elif i == Types.Optional:
        if json is None:
            return Compressed('~', True)
        else:
            ret = data_compress(sig[1], json, state)
            if ret.null_head:
                return ret.prepend('!')
            return ret

    elif json is None or i == Types.Null:
        if json is None and i == Types.Null:
            return Compressed('~', True)

        if json is None:
            raise Exception('Schema mismatch, missing field in object.')
        if i == Types.Null:
            raise Exception('Schema mismatch, required a null value.')

    elif i == Types.String:
        v = state.put(json)
        if v < 0:
            return Compressed('"' + json.replace('\\', '\\\\').replace('"', '\\"') + '"')
        else:
            return Compressed('*%d' % v)

    elif i == Types.Int:
        v = state.put(json)
        if v < 0:
            return Compressed('#%d' % json)
        else:
            return Compressed('*%d' % v)

    elif i == Types.Real:
        v = state.put(json)
        if v < 0:
            return Compressed('#%g' % json)
        else:
            return Compressed('*%d' % v)

    elif i == Types.Bool:
        if json:
            return Compressed('T')
        else:
            return Compressed('F')

    else:
        raise Exception('Sanity error, invalid signature: ' + str(sig))

def data_uncompress(sig, tokenizer, state):
    i = sig[0]

    if i == Types.Bool:
        if tokenizer.consume('T'):
            return True
        elif tokenizer.consume('F'):
            return False
        else:
            raise Exception('Schema/data mismatch while reading Bool.')

    elif i == Types.Real or i == Types.Int:
        typ = (int if i == Types.Int else float)

        if tokenizer.consume('#'): 
            v = tokenizer.skipto('TF[]~"*#!')
            ret = typ(v)
        elif tokenizer.consume('*'):
            ret = state.get(typ, int(tokenizer.pop()))
        else:
            raise Exception('Schema/data mismatch while reading Int or Real.')

        state.put_raw(ret)
        return ret

    elif i == Types.String:
        if tokenizer.consume('"'):
            ret = ''
            while True:
                ret += tokenizer.skipto('"\\')
                if tokenizer.consume('"'):
                    break
                elif tokenizer.consume('\\'):
                    if tokenizer.consume('"'):
                        ret += '"'
                    elif tokenizer.consume('\\'):
                        ret += '\\'
                    else:
                        raise Exception('Invalid quoted char in string.')
                else:
                    raise Exception('Premature end of string.')
        elif tokenizer.consume('*'):
            ret = state.get(str, int(tokenizer.pop()))
        else:
            raise Exception('Premature end of string.')

        state.put_raw(ret)
        return ret

    elif i == Types.Null:
        if not tokenizer.consume('~'):
            raise Exception('Schema/data mismatch while reading Null.')

    elif i == Types.Optional:
        if tokenizer.consume('~'):
            return None
        # This non-mandatory symbol is used to mark that an optional object is present.
        tokenizer.consume('!')
        return data_uncompress(sig[1], tokenizer, state)

    elif i == Types.List:
        if not tokenizer.consume('['):
            raise Exception('Schema/data mismatch while reading List.')

        ret = []
        while not tokenizer.consume(']'):
            v = data_uncompress(sig[1], tokenizer, state)
            ret.append(v)

        return ret

    elif i == Types.Object:

        ret = {}
        for _, k, t in sig[1]:
            v = data_uncompress(t, tokenizer, state)
            if v is not None:
                ret[k] = v

        return ret

    else:
        raise Exception('Sanity error, not a signature in data_uncompress().')
            

    
def compress(sig, json):
    if type(sig) != tuple:
        sig = signature_parse(Tokenizer(sig))
    state = State()
    data = data_compress(sig, json, state).data
    return signature_print(sig) + '\n' + data

def uncompress(data):
    data = Tokenizer(data)
    sig = signature_parse(data)
    data.consume('\n')
    state = State()
    data = data_uncompress(sig, data, state)
    return sig, data

def test(json1):

    sig = signature_infer(json1)
    compressed = compress(sig, json1)

    sig2, json2 = uncompress(compressed)

    if sig2 != sig or signature_print(sig2) != signature_print(sig):
        raise Exception('Could not roundtrip signature: ' + signature_print(sig))

    if compressed != compress(sig, json2):
        print(json1)
        print(json2)
        raise Exception('Could not roundtrip data: ' + str(json2))

    print(compressed)

if __name__=='__main__':
    test(json.load(sys.stdin))
