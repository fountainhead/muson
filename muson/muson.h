#ifndef __MUSON_H
#define __MUSON_H

#include <string.h>

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>
#include <list>
#include <type_traits>

/*
 
Public API:

    using T = muson::Signature<LIT("...")>;    // Given a muson signature, compose a concrete C++ type.
    static const std::string Tsig = T::print_signature();   // Return a signature from a concrete C++ type.
    muson::decode(Tsig, R, t);    // R is a subclass of muson::Reader<Iter>.
    muson::decode(Tsig, data, t);    // data is an std::string.
    muson::encode(Tsig, W, t);    // W is an object that supports operator<< for output.

T is a composition of types that are layout-compatible to these structs:

    struct Null {};
    struct Bool { bool v; };
    struct Int { int64_t v; };
    struct Real { double v; };
    struct String { std::string v; };
    struct Optional<T> { bool null; T v; };
    struct List<T> { std::vector<T> v; };
    struct Object<T...> { T... v...; };
*/


namespace muson {

/* 
   Compile-time strings.
   Preprocessor HACKs, too bad C++11 didn't support this natively.
*/

template<char... C>
struct Literal {
private:
    static constexpr const char vals[sizeof...(C)+1] = { C...,'\0' };
public:
    
    static constexpr const char* data() {
        return &vals[0];
    }
};

template<char... C>
constexpr const char Literal<C...>::vals[sizeof...(C)+1];

// Remove trailing zero bytes.

template <typename T, typename S> struct CleanUp;

template <char... C, char... D>
struct CleanUp<Literal<C...>, Literal<'\0', D...>> {
    using type = Literal<C...>;
};

template <char... C, char X, char... D>
struct CleanUp<Literal<C...>, Literal<X, D...>> {
    using type = typename CleanUp<Literal<C..., X>, Literal<D...>>::type;
};

template <char... C>
using Cleaned = typename CleanUp<Literal<>, Literal<C...>>::type;

// Macro crap for generating templates from string literals.

inline constexpr int add(int a, int b) { return a + b; }

template<int N, int M>
inline constexpr char index(const char (&c)[M]) {
    return c[N < M ? N : M-1];
}

#define __LITIDX1(n,l) ::muson::index<::muson::add(n,0)>(l), ::muson::index<::muson::add(n,1)>(l)
#define __LITIDX2(n,l) __LITIDX1(::muson::add(n,0),l), __LITIDX1(::muson::add(n,2),l)
#define __LITIDX3(n,l) __LITIDX2(::muson::add(n,0),l), __LITIDX2(::muson::add(n,4),l)
#define __LITIDX4(n,l) __LITIDX3(::muson::add(n,0),l), __LITIDX3(::muson::add(n,8),l)
#define __LITIDX5(n,l) __LITIDX4(::muson::add(n,0),l), __LITIDX4(::muson::add(n,16),l)
#define __LITIDX6(n,l) __LITIDX5(::muson::add(n,0),l), __LITIDX5(::muson::add(n,32),l)
#define __LITIDX7(n,l) __LITIDX6(::muson::add(n,0),l), __LITIDX6(::muson::add(n,64),l)
#define __LITIDX8(n,l) __LITIDX7(::muson::add(n,0),l), __LITIDX7(::muson::add(n,128),l)
#define LETTERS(l) __LITIDX8(0,l)
#define LIT(l) ::muson::Cleaned<LETTERS(l)>

/*
  Compile-time signature types.
  These serve as runtime data holders when instantiated.
*/

struct Null {

    static std::string print_signature() {
        return "Null";
    }
};

struct Bool {
    bool data;

    static std::string print_signature() {
        return "Bool";
    }
};

struct Int {
    int64_t data;

    static std::string print_signature() {
        return "Int";
    }
};

struct Real {
    double data;

    static std::string print_signature() {
        return "Real";
    }
};

struct String {
    std::string data;

    static std::string print_signature() {
        return "String";
    }
};

template <typename T>
struct Optional {
    using type = T;

    bool null;
    T data;

    static std::string print_signature() {
        return "?" + type::print_signature();
    }
};

template <typename T>
struct List {
    using type = T;

    std::vector<T> data;

    static std::string print_signature() {
        return "[" + type::print_signature() + "]";
    }
};

template <typename T>
struct Object {
    using type = T;

    T data;

    static std::string print_signature() {
        return "{" + type::print_signature() + "}";
    }
};

template <typename TAG, typename T>
struct Pair {
    using tag = TAG;
    using type = T;

    T data;

    static std::string print_signature() {
        return tag::data() + (":" + type::print_signature());
    }
};

template <typename... T> struct Seq;

template <typename T>
struct Seq<T> {
    using type = T;

    T data;

    static std::string print_signature() {
        return type::print_signature();
    }
};

template <typename T, typename X, typename... S>
struct Seq<T, X, S...> {
    using type = T;
    using tail_type = Seq<X, S...>;

    T data;
    Seq<X, S...> tail;

    static std::string print_signature() {
        return type::print_signature() + "," + tail_type::print_signature();
    }
};

/*
  Compile-time signature parsing.
*/

template <typename T> struct SigParser;

template <char... C>
struct SigParser<Literal<'N','u','l','l', C...>> {
    using type = Null;
    using tail = Literal<C...>;
};

template <char... C>
struct SigParser<Literal<'B','o','o','l', C...>> {
    using type = Bool;
    using tail = Literal<C...>;
};

template <char... C>
struct SigParser<Literal<'I','n','t', C...>> {
    using type = Int;
    using tail = Literal<C...>;
};

template <char... C>
struct SigParser<Literal<'R','e','a','l', C...>> {
    using type = Real;
    using tail = Literal<C...>;
};

template <char... C>
struct SigParser<Literal<'S','t','r','i','n','g', C...>> {
    using type = String;
    using tail = Literal<C...>;
};

template <char... C>
struct SigParser<Literal<'?', C...>> {
    using call = SigParser<Literal<C...>>;
    using type = Optional<typename call::type>;
    using tail = typename call::tail;
};

template <char C, typename T> struct Expect;

template <char C, char... D>
struct Expect<C, Literal<C, D...>> {
    using type = Literal<D...>;
};

template <char... C>
struct SigParser<Literal<'[', C...>> {
    using call = SigParser<Literal<C...>>;
    using type = List<typename call::type>;
    using tail = typename Expect<']', typename call::tail>::type;
};


template <typename T, typename S> struct PairParser;

template <char... C, char... D>
struct PairParser<Literal<D...>, Literal<':', C...>> {
    using name = Literal<D...>;
    using call = SigParser<Literal<C...>>;
    using type = Pair<name, typename call::type>;
    using tail = typename call::tail;
};

template <char C, char... Cs, char... D>
struct PairParser<Literal<D...>, Literal<C, Cs...>> {
    using call = PairParser<Literal<D..., C>, Literal<Cs...>>;
    using type = typename call::type;
    using tail = typename call::tail;
};

template <typename T, typename S> struct SeqParser;

template <typename T, char... C>
struct SeqParser<T, Literal<'}', C...>> {
    using type = T;
    using tail = Literal<C...>;
};

template <typename... T, char... C>
struct SeqParser<Seq<T...>, Literal<',', C...>> {
    using pair = PairParser<Literal<>, Literal<C...>>;
    using cons = Seq<T..., typename pair::type>;
    using call = SeqParser<cons, typename pair::tail>;
    using type = typename call::type;
    using tail = typename call::tail;
};

template <char... C>
struct SigParser<Literal<'{', C...>> {
    using pair = PairParser<Literal<>, Literal<C...>>;
    using call = SeqParser<Seq<typename pair::type>, typename pair::tail>;
    using type = Object<typename call::type>;
    using tail = typename call::tail;
};

template <char... C>
struct SigParser<Literal<'\0', C...>> {
};

template <typename L>
using Signature = typename SigParser<L>::type;

/*
  Runtime data parsing.
*/

template <typename T>
struct Cache {
    std::list<T> items;

    void put_raw(const T& v, size_t n) {

        items.emplace_front(v);

        if (n >= 10) {
            items.pop_back();
        }
    }

    int put(const T& v) {
        auto i = items.begin();
        auto e = items.end();
        size_t n = 0;

        while (i != e) {
            if (*i == v) {
                items.splice(items.begin(), items, i);
                return n;
            }
            ++i;
            ++n;
        }

        put_raw(v, n);

        return -1;
    }

    const T& get(size_t x) {
        auto i = items.begin();
        auto e = items.end();
        size_t n = 0;

        while (i != e) {
            if (n == x) {
                items.splice(items.begin(), items, i);
                return *(items.begin());
            }
            ++i;
            ++n;
        }

        throw std::runtime_error("Invalid backreference.");
    }
};

template <typename I>
struct Reader {
    I i;
    I e;

    Reader(I _i, I _e) : i(_i), e(_e) {}

    bool consume(const char* pref) {
        I j = i;

        while (1) {
            if (*pref == '\0') {
                i = j;
                return true;
            }

            if (j == e) return false;

            if (*j == *pref) {
                ++j;
                ++pref;
            } else {
                return false;
            }
        }
    }

    I skipto_special() {
        I ret = i;
        while (i != e && !(*i == '#' || *i == '*' || *i == '"' || *i == '~' ||
                           *i == '[' || *i == ']' || *i == 'T' || *i == 'F' || *i == '!')) {
            ++i;
        }

        return ret;
    }

    I skipto_quote() {
        I ret = i;
        while (i != e && !(*i == '"' || *i == '\\')) {
            ++i;
        }

        if (i == e)
            throw std::runtime_error("Premature end of input data.");

        return ret;
    }

    char pop() {
        if (i == e)
            throw std::runtime_error("Premature end of input data.");

        char ret = *i;
        ++i;
        return ret;
    }

    void error(const std::string& what) {
        throw std::runtime_error("Invalid data while reading " + what + ", unparsed data: " + std::string(i, e));
    }

    int64_t to_int(const I& b, const I& e) {
        return ::strtoull(&(*b), NULL, 10);
    }

    double to_real(const I& b, const I& e) {
        return ::strtod(&(*b), NULL);
    }

    Cache<int64_t> int_cache;
    Cache<double> real_cache;
    Cache<std::string> string_cache;

    void put(int64_t v) { int_cache.put_raw(v, int_cache.items.size()); }
    void put(double v) { real_cache.put_raw(v, real_cache.items.size()); }
    void put(const std::string& v) { string_cache.put_raw(v, string_cache.items.size()); }

    int64_t int_get(size_t x) { return int_cache.get(x); }
    double real_get(size_t x) { return real_cache.get(x); }
    const std::string& string_get(size_t x) { return string_cache.get(x); }
};

template <typename BUF, typename T> inline void read(BUF&, T&);

template <typename BUF>
inline void read(BUF& buf, Null& v) {

    if (!buf.consume("~")) {
        buf.error("Null");
    }
}

template <typename BUF>
inline void read(BUF& buf, Bool& v) {

    if (buf.consume("T")) {
        v.data = true;
    } else if (buf.consume("F")) {
        v.data = false;
    } else {
        buf.error("Bool");
    }
}

template <typename BUF>
inline void read(BUF& buf, Int& v) {

    if (buf.consume("#")) {
        v.data = buf.to_int(buf.skipto_special(), buf.i);
        buf.put(v.data);

    } else if (buf.consume("*")) {
        v.data = buf.int_get(buf.pop() - '0');

    } else {
        buf.error("Int");
    }
}

template <typename BUF>
inline void read(BUF& buf, Real& v) {

    if (buf.consume("#")) {
        v.data = buf.to_real(buf.skipto_special(), buf.i);
        buf.put(v.data);

    } else if (buf.consume("*")) {
        v.data = buf.real_get(buf.pop() - '0');

    } else {
        buf.error("Real");
    }
}

template <typename BUF>
inline void read(BUF& buf, String& v) {

    if (buf.consume("\"")) {

        while (1) {
            auto i = buf.skipto_quote();

            v.data.append(i, buf.i);

            if (buf.consume("\"")) {
                break;
            }

            ++buf.i;

            if (buf.consume("\"")) {
                v.data += '"';
            } else if (buf.consume("\\")) {
                v.data += '\\';
            } else {
                buf.error("String");
            }
        }

        buf.put(v.data);

    } else if (buf.consume("*")) {
        v.data = buf.string_get(buf.pop() - '0');

    } else {
        buf.error("String");
    }
}

template <typename BUF, typename T>
inline void read(BUF& buf, Optional<T>& v) {

    if (buf.consume("~")) {
        v.null = true;

    } else {
        buf.consume("!");
        v.null = false;
        read(buf, v.data);
    }
}

template <typename BUF, typename T>
inline void read(BUF& buf, List<T>& v) {

    if (!buf.consume("[")) {
        buf.error("List");
    }

    while (!buf.consume("]")) {
        v.data.emplace_back();
        read(buf, v.data.back());
    }
}

template <typename BUF, typename T>
inline void read(BUF& buf, Object<T>& v) {
    read(buf, v.data);
}

template <typename BUF, typename T>
inline void read(BUF& buf, Seq<T>& v) {
    read(buf, v.data);
}

template <typename BUF, typename T, typename X, typename... S>
inline void read(BUF& buf, Seq<T, X, S...>& v) {
    read(buf, v.data);
    read(buf, v.tail);
}

template <typename BUF, typename TAG, typename T>
inline void read(BUF& buf, Pair<TAG,T>& v) {
    read(buf, v.data);
}

template <typename T>
using is_not_a_string = typename std::enable_if<!std::is_same<T,std::string>::value &&
                                                !std::is_same<T,const std::string>::value>::type;

template <typename R, typename T, typename = is_not_a_string<R>>
inline void decode(const std::string& signature, R& buf, T& v) {

    if (!buf.consume(signature.c_str())) 
        throw std::runtime_error("Invalid signature in data.");

    buf.consume("\n");

    read(buf, v);
}

template <template <typename> class R = Reader, typename T>
inline void decode(const std::string& signature, const std::string& data, T& v) {

    R<std::string::const_iterator> buf(data.begin(), data.end());

    if (!buf.consume(signature.c_str())) 
        throw std::runtime_error("Invalid signature in data.");

    buf.consume("\n");

    read(buf, v);
}

/*
  Disambiguation of optionals
 */

template <typename T>
inline bool exclam(const T&)
{
    return false;
}

inline bool exclam(const Null&)
{
    return true;
}

template <typename T>
inline bool exclam(const Optional<T>& v)
{
    return v.null || exclam(v.data);
}


template <typename T>
inline bool exclam(const Object<T>& v) {
    return exclam(v.data);
}

template <typename T, typename... S>
inline bool exclam(const Seq<T, S...>& v) {
    return exclam(v.data);
}

template <typename TAG, typename T>
inline bool exclam(const Pair<TAG,T>& v) {
    return exclam(v.data);
}

/*
  Runtime data encoding.
*/

template <typename BUF>
struct Writer {
    BUF& b;

    Writer(BUF& _b) : b(_b) {}

    template <typename T>
    Writer<BUF>& operator<<(const T& t) {
        b << t;
        return *this;
    }

    Cache<int64_t> int_cache;
    Cache<double> real_cache;
    Cache<std::string> string_cache;

    int put(int64_t v) { return int_cache.put(v); }
    int put(double v) { return real_cache.put(v); }
    int put(const std::string& v) { return string_cache.put(v); }
};

template <typename BUF, typename T> inline void write(BUF&, const T&);

template <typename BUF>
inline void write(BUF& buf, const Null& v) {
    buf << '~';
}

template <typename BUF>
inline void write(BUF& buf, const Bool& v) {
    if (v.data) {
        buf << 'T';
    } else {
        buf << 'F';
    }
}

template <typename BUF>
inline void write(BUF& buf, const Int& v) {
    int n = buf.put(v.data);

    if (n >= 0) {
        buf << '*';
        buf << n;
    } else {
        buf << '#';
        buf << v.data;
    }
}

template <typename BUF>
inline void write(BUF& buf, const Real& v) {
    int n = buf.put(v.data);

    if (n >= 0) {
        buf << '*';
        buf << n;
    } else {
        buf << '#';
        buf << v.data;
    }
}

template <typename BUF>
inline void write(BUF& buf, const String& v) {
    int n = buf.put(v.data);

    if (n >= 0) {
        buf << '*';
        buf << n;
    } else {
        buf << '"';
        for (char c : v.data) {
            if (c == '"' || c == '\\') {
                buf << '\\';
            }
            buf << c;
        }
        buf << '"';
    }
}

template <typename BUF, typename T>
inline void write(BUF& buf, const Optional<T>& v) {

    if (v.null) {
        buf << '~';
    } else {
        if (exclam(v.data)) {
            buf << '!';
        }
        write(buf, v.data);
    }
}

template <typename BUF, typename T>
inline void write(BUF& buf, const List<T>& v) {

    buf << '[';
    for (const auto& i : v.data) {
        write(buf, i);
    }
    buf << ']';
}

template <typename BUF, typename T>
inline void write(BUF& buf, const Object<T>& v) {
    write(buf, v.data);
}

template <typename BUF, typename T>
inline void write(BUF& buf, const Seq<T>& v) {
    write(buf, v.data);
}

template <typename BUF, typename T, typename X, typename... S>
inline void write(BUF& buf, const Seq<T, X, S...>& v) {
    write(buf, v.data);
    write(buf, v.tail);
}

template <typename BUF, typename TAG, typename T>
inline void write(BUF& buf, const Pair<TAG,T>& v) {
    write(buf, v.data);
}

template <typename BUF, typename T>
inline void encode(const std::string& signature, BUF& _buf, const T& v) {

    Writer<BUF> buf(_buf);
    buf << signature;
    buf << '\n';
    write(buf, v);
}


/*
  Accesors for object values.
  You can cast to a 'struct' of equivalent layout instead.
*/

template <typename TAG, typename T> struct TypeOf {
    using type = void;
};

template <typename TAG, typename T, typename... S>
struct TypeOf<TAG, const Seq<Pair<TAG, T>, S...>> {
    using type = const T&;
};

template <typename TAG, typename T, typename... S>
struct TypeOf<TAG, Seq<Pair<TAG, T>, S...>> {
    using type = T&;
};

template <typename TAG, typename T, typename... S>
struct TypeOf<TAG, const Seq<T, S...>> {
    using type = typename TypeOf<TAG, const Seq<S...>>::type;
};

template <typename TAG, typename T, typename... S>
struct TypeOf<TAG, Seq<T, S...>> {
    using type = typename TypeOf<TAG, Seq<S...>>::type;
};

////

template <typename TAG, typename T, typename... S>
inline T& get(Seq<Pair<TAG, T>, S...>& obj) {
    return obj.data.data;
}

template <typename TAG, typename T, typename... S>
inline auto get(Seq<T, S...>& obj) -> typename TypeOf<TAG, Seq<S...>>::type {
    return get<TAG>(obj.tail);
}
                                                               
template <typename TAG, typename T>
inline auto get(Object<T>& obj) -> typename TypeOf<TAG, T>::type {
    return get<TAG>(obj.data);
}

template <typename TAG, typename T, typename... S>
inline const T& get(const Seq<Pair<TAG, T>, S...>& obj) {
    return obj.data.data;
}

template <typename TAG, typename T, typename... S>
inline auto get(const Seq<T, S...>& obj) -> typename TypeOf<TAG, const Seq<S...>>::type {
    return get<TAG>(obj.tail);
}
                                                               
template <typename TAG, typename T>
inline auto get(const Object<T>& obj) -> typename TypeOf<TAG, const T>::type {
    return get<TAG>(obj.data);
}

#define MUGET(x, l) ::muson::get<LIT(l)>(x)

// Needed to get rid of type aliasing warnings.

template <typename T, typename S>
T& cast(S& v) {
    void* x = (void*)(&v);
    return *(T*)x;
}

}

#endif
