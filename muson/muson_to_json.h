#ifndef __MUSON_TO_JSON_H
#define __MUSON_TO_JSON_H

#include "muson.h"

namespace muson {

template <typename T> inline bool exists(const T&) { return true; }

template <typename T>
inline bool exists(const Optional<T>& v) { return !v.null; }

template <typename TAG, typename T>
inline bool exists(const Pair<TAG, T>& v) { return exists(v.data); }

template <typename T>
inline bool exists(const Seq<T>& v) { return exists(v.data); }

template <typename T, typename X, typename... S>
inline bool exists(const Seq<T, X, S...>& v) { return exists(v.data) || exists(v.tail); }

/* Convert to string representation. */

template <typename BUF, typename T> inline void to_string_json(BUF&, const T&);

template <typename BUF>
inline void to_string_json(BUF& buf, const Null& v) { buf << "null"; }

template <typename BUF>
inline void to_string_json(BUF& buf, const Bool& v) {
    if (v.data) {
        buf << "true";
    } else {
        buf << "false";
    }
}

template <typename BUF>
inline void to_string_json(BUF& buf, const Int& v) { buf << v.data; }

template <typename BUF>
inline void to_string_json(BUF& buf, const Real& v) { buf << v.data; }

template <typename BUF>
inline void to_string_json(BUF& buf, const String& v) {
    buf << '"';
    for (unsigned char c : v.data) {
        switch (c) {
        case '\\':
            buf << "\\\\";
            break;
        case '\b':
            buf << "\\b";
            break;
        case '\f':
            buf << "\\f";
            break;
        case '\n':
            buf << "\\n";
            break;
        case '\r':
            buf << "\\r";
            break;
        case '\t':
            buf << "\\t";
            break;
        case '"':
            buf << "\\\"";
            break;
        default:
            buf << c;
        }
    }
    buf << '"';
}

template <typename BUF, typename T>
inline void to_string_json(BUF& buf, const Optional<T>& v) {
    if (v.null) {
        buf << "null";
    } else {
        to_string_json(buf, v.data);
    }
}

template <typename BUF, typename T>
inline void to_string_json(BUF& buf, const List<T>& v) {
    buf << "[";
    bool first = true;
    for (const auto& i : v.data) {
        if (!exists(i))
            continue;

        if (first) {
            first = false;
        } else {
            buf << ",";
        }

        to_string_json(buf, i);
    }
    buf << "]";
}

template <typename BUF, typename T>
inline void to_string_json(BUF& buf, const Seq<T>& v) {
    to_string_json(buf, v.data);
}

template <typename BUF, typename T, typename X, typename... S>
inline void to_string_json(BUF& buf, const Seq<T, X, S...>& v) {
    bool a = exists(v.data);
    bool b = exists(v.tail);

    if (a) {
        to_string_json(buf, v.data);
    }

    if (a && b) {
        buf << ",";
    }

    if (b) {
        to_string_json(buf, v.tail);
    }
}

template <typename BUF, typename TAG, typename T>
inline void to_string_json(BUF& buf, const Pair<TAG, T>& v) {
    buf << "\"" << TAG::data() << "\":";
    to_string_json(buf, v.data);
}
    
template <typename BUF, typename T>
inline void to_string_json(BUF& buf, const Object<T>& v) {
    buf << "{";
    to_string_json(buf, v.data);
    buf << "}";
}

}

#endif
